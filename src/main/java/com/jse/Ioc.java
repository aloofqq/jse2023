package com.jse;

import java.util.concurrent.ConcurrentHashMap;

public class Ioc {

	public final static ConcurrentHashMap<String,Object> BEANS=new ConcurrentHashMap<>();
	
	public static <T> T get(String name) {return (T)BEANS.get(name);}
	public static <T> T get(String name,Class<T> t) {return get(name);}
	
	public static Object reg(String name,Object bean) {
		return BEANS.putIfAbsent(name, bean);
	}
}
