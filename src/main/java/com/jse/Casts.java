package com.jse;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.function.IntFunction;

public class Casts {

	public static Integer toInt(Object o) {
		if(o==null)return null;
		else if(o instanceof Number n)return n.intValue();
		return Integer.valueOf(o.toString());
	}
	public static Short toShort(Object o) {
		if(o==null)return null;
		else if(o instanceof Number n)return n.shortValue();
		return Short.valueOf(o.toString());
	}
	public static Long toLong(Object o) {
		if(o==null)return null;
		else if(o instanceof Number n)return n.longValue();
		return Long.valueOf(o.toString());
	}
	public static Float toFloat(Object o) {
		if(o==null)return null;
		else if(o instanceof Number n)return n.floatValue();
		return Float.valueOf(o.toString());
	}
	public static Double toDouble(Object o) {
		if(o==null)return null;
		else if(o instanceof Number n)return n.doubleValue();
		return Double.valueOf(o.toString());
	}
	public static Byte toByte(Object o) {
		if(o==null)return null;
		else if(o instanceof Number n)return n.byteValue();
		return Byte.valueOf(o.toString());
	}
	public static Character toChar(Object o) {
		if(o==null)return null;
		else if(o instanceof Number n)return Character.valueOf((char)n.intValue());
		return Character.valueOf(o.toString().charAt(0));
	}
	public static Boolean toBoolean(Object o) {
		if(o==null)return null;
		else if(o instanceof Boolean b)return b;
		else if(o instanceof Number n)return n.intValue()!=0;//非零值为true，0则为false
		return Boolean.valueOf(o.toString());
	}
	public static String toString(Object o) {
		if(o==null)return null;
		
		return o.toString();
	}
	public static <T>Object to(Object o,Class<T> target) {
		if(target==null)return null;
		if(target.isPrimitive()&&o==null){if(boolean.class==target)return Boolean.FALSE;return 0;}
		if(o==null)return null;
		var oc=o.getClass();
		if(target==oc)return o;
		if(target.isAssignableFrom(oc))return o;//相等或子类无需转换
		if(target==int.class||target==Integer.class)return toInt(o);
		if(target==short.class||target==Short.class)return toShort(o);
		if(target==long.class||target==Long.class)return toLong(o);
		if(target==float.class||target==Float.class)return toFloat(o);
		if(target==double.class||target==Double.class)return toDouble(o);
		if(target==byte.class||target==Byte.class)return toByte(o);
		if(target==char.class||target==Character.class)return toChar(o);
		if(target==boolean.class||target==Boolean.class)return toBoolean(o);
		if(target==String.class)return o.toString();
		if(target==Date.class)return Times.parse(o);
		if(target.isArray()) {
			var c=target.getComponentType();
			if(oc.isArray()){
				Object[] l=(Object[])o;
				int len=l.length;
				Object[] arr=(Object[])Array.newInstance(c,len);
				for(int i=0;i<len;i++) {
					arr[i]=to(l[i],c);
				}
				return arr;
			}else if(o instanceof Collection l){
				int len=l.size();
				Object[] arr=(Object[])Array.newInstance(c,len);
				int i=0;
				for (Object v : l) {
					arr[i]=to(v,c);
					i++;
				}
				return arr;
			}else {
				System.out.println(o.getClass()+"不支持");
				return null;
			}
		}
		return null;
	}
	
	

	public static void main(String[] args) {
		String[] s=(String[])to(Lang.ofSet("a",true,2),String[].class);
		System.out.println(Arrays.toString(s));
	}
}
