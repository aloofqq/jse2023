package com.jse.web;

import java.util.HashMap;
import java.util.Map;

public class HttpSession {

	private String id;
	private long creationTime;
	private long lastAccessedTime;
	private int maxInactiveInterval;
	private HashMap<String,Object> attributes=new HashMap<>();
	
	public HttpSession(String id) {
		this.id=id;
		creationTime=System.currentTimeMillis();
	}
	
	public String getId() {return id;}
	
	public long getCreationTime() {return creationTime;}
	public long getLastAccessedTime() {return lastAccessedTime;}
	public void setMaxInactiveInterval(int interval) {
		this.maxInactiveInterval=interval;
	}
	public int getMaxInactiveInterval(){return maxInactiveInterval;}
	public Object getAttribute(String name) {
		return attributes.get(name);
	}
	public void setAttribute(String name, Object value) {
		attributes.put(name, value);
	}
	public void removeAttribute(String name) {
		attributes.remove(name);
	}
	public void invalidate() {
		attributes.clear();
	}
	public boolean isNew(){return lastAccessedTime==0;}

	public Map<String,Object> attributes() {return attributes;}
}
