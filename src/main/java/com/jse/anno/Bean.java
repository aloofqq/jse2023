package com.jse.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 组件对象注解,在类或配置了配置注解的方法上
 * @author dzh
 *
 */
@Target(ElementType.TYPE)  // 用于描述类、接口、enum
@Retention(RetentionPolicy.RUNTIME) // 运行时有效
@Inherited
public @interface Bean {
	/** 是否单例*/
	boolean single = true;
	
	String name() default "";
}