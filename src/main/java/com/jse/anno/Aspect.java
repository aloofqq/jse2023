package com.jse.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 拦截方法 bean为IOC内注册的,方法是否拦截可实现对应aop方法内判断
 * @author dzh
 *
 */
@Target(ElementType.TYPE)  // 用于描述类、接口、enum
@Retention(RetentionPolicy.RUNTIME) // 运行时有效
@Inherited
public @interface Aspect {
	
	/** 需拦截的bean */
	String[] beans();
}
