package com.jse.jakarta;

import jakarta.servlet.http.HttpSessionEvent;
import jakarta.servlet.http.HttpSessionIdListener;
import jakarta.servlet.http.HttpSessionListener;

public class JseWebListener implements HttpSessionListener
,HttpSessionIdListener//session id更改通知
//,HttpSessionBindingListener//保存到session中或从session失效与移除时才会触发
//,ServletRequestListener
//HttpSessionAttributeListener//只对操作方法
//HttpSessionActivationListener session 持久化
//,ServletContextAttributeListener ServletContext的属性监听
//,ServletRequestAttributeListener //request属性监听
{

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		var session=se.getSession();
		Web.SESSIONS.put(session.getId(),session);
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		var session=se.getSession();
		Web.SESSIONS.remove(session.getId());
	}

	@Override
	public void sessionIdChanged(HttpSessionEvent se, String oldSessionId) {
		var session=se.getSession();
		Web.SESSIONS.remove(oldSessionId);
		Web.SESSIONS.put(session.getId(),session);
		
	}

//	@Override public void valueBound(HttpSessionBindingEvent event) {}//attribute HttpSessionBindingListener
//	@Override public void valueUnbound(HttpSessionBindingEvent event) {}//invalidate|超时|remove|setAttribute替换

//	@Override public void requestDestroyed(ServletRequestEvent sre) {}
//	@Override public void requestInitialized(ServletRequestEvent sre) {}

//	@Override public void attributeAdded(HttpSessionBindingEvent event) {}//添加 HttpSessionAttributeListener
//	@Override public void attributeRemoved(HttpSessionBindingEvent event) {}//attributeRemoved
//	@Override public void attributeReplaced(HttpSessionBindingEvent event) {}//重新setAttribute

//	@Override public void attributeAdded(ServletRequestAttributeEvent srae) {}
//	@Override public void attributeRemoved(ServletRequestAttributeEvent srae) {}
//	@Override public void attributeReplaced(ServletRequestAttributeEvent srae) {}

//	@Override public void attributeAdded(ServletContextAttributeEvent event) {}
//	@Override public void attributeRemoved(ServletContextAttributeEvent event) {}
//	@Override public void attributeReplaced(ServletContextAttributeEvent event) {}
	
	

	
}
