package com.jse.jakarta;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpCookie;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;

import com.jse.Io;
import com.jse.Log;
import com.jse.json.JsonObject;

//import jakarta.servlet.ServletOutputStream;
//import jakarta.servlet.http.Cookie;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//import jakarta.servlet.http.HttpSession;

public class Ctx {

//	private HttpServletRequest req;
//	private HttpServletResponse res;
	
	public String path;
	public String suffix;
	public String method;
	
	private String body;//禁止直接访问
	private JsonObject tbl;//需根据初始化
	private InputStream in;
	
	
	public Ctx(String method,String path,InputStream in) {
//		this.req=req;
//		this.res=res;
		this.method=method;
		this.path=path;
		int suffixindex=path.lastIndexOf('.');
		suffix=suffixindex==-1?"":path.substring(suffixindex+1);
	}
	
	
	public String body() {//针对字符
		if(body!=null)return body;
		body=Io.readString(in);
		return body;
	}
	
	public JsonObject tbl() {
		if(tbl!=null)return tbl;
		return null;
	}
	
//	public String getHeader(String name) {return req.getHeader(name);}
//	public void setAttribute(String name,Object value) {req.setAttribute(name,value);}
//	public Object getAttribute(String name) {return req.getAttribute(name);}
//	public Object getSession() {return req.getSession();}
//	public String getSessionId() {return req.getRequestedSessionId();}
//	public String getRequestId() {return req.getRequestId();}
//	public Cookie[] cookies() {return req.getCookies();}
//	public Cookie cookie(String name) {return Arrays.stream(req.getCookies()).filter(c->name.equals(c.getName())).findAny().get();}
//	
//	public void setResponseHeader(String name,String value) {res.setHeader(name, value);}
//	public void setResponseDateHeader(String name,long value) {res.setDateHeader(name, value);}
//	public void setResponseIntHeader(String name,int value) {res.setIntHeader(name, value);}
//	public void addResponseHeader(String name,String value) {res.addHeader(name, value);}
//	public void addResponseDateHeader(String name,long value) {res.addDateHeader(name, value);}
//	public void addResponseIntHeader(String name,int value) {res.addIntHeader(name, value);}
//	public String getResponseHeader(String name) {return res.getHeader(name);}
//	public Collection<String> getResponseHeaders(String name) {return res.getHeaders(name);}
//	public Collection<String> getResponseHeaderNames() {return res.getHeaderNames();}
	
	
//	public void addCookie(Cookie cookie) {res.addCookie(cookie);}
//	public void addCookie(HttpCookie hcookie) {
//		Cookie cookie=new Cookie(hcookie.getName(),hcookie.getValue());
//		if(hcookie.getSecure())cookie.setSecure(true);
//		if(hcookie.getDomain()!=null)cookie.setDomain(hcookie.getDomain());
//		if(hcookie.getMaxAge()>-1)cookie.setMaxAge((int)hcookie.getMaxAge());
//		if(hcookie.getPath()!=null)cookie.setPath(hcookie.getPath());
//		if(hcookie.isHttpOnly())cookie.setHttpOnly(true);
//		res.addCookie(cookie);
//	}
//	public void addCookie(Map<String,Object> hcookie) {
//		Cookie cookie=new Cookie(hcookie.get("name").toString(),hcookie.get("value").toString());
//		if(Boolean.TRUE==hcookie.get("secure"))cookie.setSecure(true);
//		if(hcookie.get("domain")!=null)cookie.setDomain(hcookie.get("domain").toString());
//		if(hcookie.get("maxAge")!=null)cookie.setMaxAge((int)hcookie.get("maxAge"));
//		if(hcookie.get("path")!=null)cookie.setPath(hcookie.get("path").toString());
//		if(Boolean.TRUE==hcookie.get("httpOnly"))cookie.setHttpOnly(true);
//		res.addCookie(cookie);
//	}
//	public String getContentType() {return res.getContentType();}
//	public void setContentType(String type) {res.setContentType(type);}
//	public void setStatus(int status) {res.setStatus(status);};
//	public void sendError(int status) {sendError(status,null);};
//	public void sendError(int status,String msg) {try {if(msg==null)res.sendError(status);else res.sendError(status,msg);} catch (IOException e) {Log.get("Web").warn("{} sendError err:{}",path,status);}};
//	public PrintWriter writer() {try {return res.getWriter();} catch (IOException e) {throw new RuntimeException(e);}}
//	public void println(CharSequence s){writer().println(s);}
//	public void append(CharSequence c){writer().append(c);}
//	public OutputStream out() {try {return res.getOutputStream();} catch (IOException e) {throw new RuntimeException(e);}}
//	public void write(byte[] b){try {out().write(b);} catch (IOException e) {throw new RuntimeException(e);}}
//	public void write(String c){try {res.getOutputStream().print(c);} catch (IOException e) {throw new RuntimeException(e);}}
//	public void writeln(String c){try {res.getOutputStream().println(c);} catch (IOException e) {throw new RuntimeException(e);}}
//	public void flushBuffer(){try {res.flushBuffer();} catch (IOException e) {throw new RuntimeException(e);}}
//	public boolean isCommitted() {return res.isCommitted();}
}
