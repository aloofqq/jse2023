package com.jse.jakarta;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.jse.Awts;
import com.jse.Fs;
import com.jse.Http;
import com.jse.Jse;
import com.jse.Lang;
import com.jse.json.Json;
import com.jse.tpl.Tpl;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public interface View {
	
	void render(HttpServletRequest req,HttpServletResponse resp,Object o) throws ServletException,IOException;

	View captcha=(req,resp,code)->{
		resp.setContentType("image/jpeg");
		resp.setHeader("Pragma", "No-cache");
		resp.setHeader("Cache-Control", "no-cache");
		resp.setDateHeader("Expires", 0);
		req.getSession().setAttribute("captcha",code);
		Awts.captcha(code.toString(),resp.getOutputStream());
	};
	
	View not=(req,resp,o)->{
		resp.sendError(404);
	};
	View err=(req,resp,o)->{
		resp.sendError(500,o.toString());
	};
	View redirect=(req,resp,url)->{
		resp.setStatus(302);
		resp.setHeader("Location",url.toString());
	};
	View forward=(req,resp,url)->{req.getRequestDispatcher(url.toString()).forward(req, resp);};
	View staticforward=(req,resp,url)->{
		var path=Path.of(Jse.webapp(req.getServerName()),req.getServletPath());
		var is=Files.exists(path);
		if(!is)path=Path.of(Jse.webapp(req.getServerName()),"static",req.getServletPath());
		if(Files.exists(path)){
			long ml=System.currentTimeMillis();
			resp.addDateHeader("Last-Modified",ml);
			resp.addHeader("Expires",new Date(ml+864000000L).toString());
			resp.setContentType(Fs.mimeType(path,"text/plain"));
			String ifModifiedSince = req.getHeader("If-Modified-Since");
			var ftime=Files.getLastModifiedTime(path);
			var lastModified=ftime.toMillis();
	        ZonedDateTime last = ftime.toInstant().atZone(ZoneId.systemDefault());
	        String eTag = "\"" + lastModified + "-" + path.hashCode()+"\"";
	        resp.setHeader("ETag", eTag);
	        if(eTag.equals(req.getHeader("If-Modified-Since")) && ifModifiedSince != null
		              && !ZonedDateTime.parse(ifModifiedSince, DateTimeFormatter.RFC_1123_DATE_TIME)
		              .isBefore(last)) {
		            resp.setStatus(304);
		            return;
		    }
	        resp.setHeader("Last-Modified", DateTimeFormatter.RFC_1123_DATE_TIME.format(last));
			Files.copy(path,resp.getOutputStream());return;
		}
		not.render(req,resp,null);
	};
	
	View tpl=(req,resp,path)->{
		resp.setContentType("text/html;charset=UTF-8");
		Tpl.use().getTemplate((String)path,false)
		.render(Web.datas(req),resp.getOutputStream());
	};

	View html=(req,resp,h)->{
		resp.setContentType("text/html;charset=UTF-8");
		try (var out = resp.getWriter()) {
			if(h==null) {
				out.write('\0');
			}else if(h instanceof Path p) {
				out.write(Fs.readString(p));
			}else{
				var s=h.toString();
				if(s.startsWith("http://")||s.startsWith("https://")) {
					out.write(Http.get(s));
				}else {
					out.write(s);
				}
			}
		}
	};

	View json=(req,resp,obj)->{
		resp.setContentType("application/json;charset=UTF-8");
		try (var out = resp.getWriter()) {
		   out.write(Json.toJson(obj));
		}
	};
	View text=(req,resp,s)->{
		resp.setContentType("text/plain;charset=UTF-8");
		try (var out = resp.getWriter()) {
		   out.write((String)s);
		}
	};
	View xml=(req,resp,s)->{
		resp.setContentType("text/xml;charset=UTF-8");
		try (var out = resp.getWriter()) {
		   out.write((String)s);
		}
	};
	
	View str=(req,resp,obj)->{//针对字符串视图,只根据返回值无其他因素
		var s=((String)obj).trim();
		var c=s.charAt(0);
		if(c=='{'||c=='[') {
			json.render(req,resp,s);
		}else if(c=='/') {
			redirect.render(req,resp,s);
		}else if(s.startsWith("<?xml")) {
			xml.render(req,resp,s);
		}else if(c=='<') {
			html.render(req,resp,s);
		}else if(s.startsWith("http://")||s.startsWith("https://")) {//代理
			html.render(req,resp,s);
		}else if(s.startsWith("->:")) {
			forward.render(req,resp,s.substring(3));
		}else if(s.startsWith(">>:")) {
			redirect.render(req,resp,s.substring(3));
		}else {
			text.render(req,resp,s);
		}
	};

	View filter=(req,resp,obj)->{//针对拦截专用视图
		if(obj==null||obj==Boolean.TRUE)return;//什么都不做放行
		if(obj instanceof String s) {//字符串视图
			str.render(req,resp,s);
		}else if(obj instanceof Map||obj instanceof Collection) {
			json.render(req,resp,obj);
		}else if(obj instanceof View v) {
			v.render(req,resp,null);
		}else html.render(req,resp,obj.toString());
	};
	
	View ret=new View() {
		@Override
		public void render(HttpServletRequest req, HttpServletResponse resp, Object o)
				throws ServletException, IOException {
			var t=Lang.def(req.getAttribute("@view"),"").toString();
			if(t==null){//没有配置视图
				if(o==null) {}
				else if(o instanceof View v) {
					v.render(req, resp, o);
				}
			}else {
				switch (t) {
					case "" ->System.out.println("");
					default ->{
						
					}
				}
			}
		}
	};
	
	/**
	 * 返回数据给客户端
	 *
	 * @param response    响应对象{@link HttpServletResponse}
	 * @param in          需要返回客户端的内容
	 * @param contentType 返回的类型，可以使用{@link FileUtil#getMimeType(String)}获取对应扩展名的MIME信息
	 *                    <ul>
	 *                      <li>application/pdf</li>
	 *                      <li>application/vnd.ms-excel</li>
	 *                      <li>application/msword</li>
	 *                      <li>application/vnd.ms-powerpoint</li>
	 *                    </ul>
	 *                    docx、xlsx 这种 office 2007 格式 设置 MIME;网页里面docx 文件是没问题，但是下载下来了之后就变成doc格式了
	 *                    参考：<a href="https://my.oschina.net/shixiaobao17145/blog/32489">https://my.oschina.net/shixiaobao17145/blog/32489</a>
	 *                    <ul>
	 *                      <li>MIME_EXCELX_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";</li>
	 *                      <li>MIME_PPTX_TYPE = "application/vnd.openxmlformats-officedocument.presentationml.presentation";</li>
	 *                      <li>MIME_WORDX_TYPE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";</li>
	 *                      <li>MIME_STREAM_TYPE = "application/octet-stream;charset=utf-8"; #原始字节流</li>
	 *                    </ul>
	 * @param fileName    文件名，自动添加双引号
	 * @since 4.1.15
	 */
//	public static void write(final jakarta.servlet.http.HttpServletResponse response, final InputStream in, final String contentType, final String fileName) {
//		final String charset = Objects.requireNonNullElse(response.getCharacterEncoding(),"UTF-8");
//		final String encodeText = URLEncoder.encode(fileName,charset);
//		response.setHeader("Content-Disposition",
//				String.format("attachment;filename=\"%s\";filename*=%s''%s", encodeText, charset, encodeText));
//		response.setContentType(contentType);
//		write(response, in);
//	}
}
