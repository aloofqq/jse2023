package com.jse;

import java.util.regex.Pattern;

public class Strings {
	
	public static Pattern CHINESE= Pattern.compile("[\u4e00-\u9fa5]");
	

	public static String firstLowerCase(String s) {
		return Character.toLowerCase(s.charAt(0))+s.substring(1);
	}
	public static String firstUpperCase(String s) {
		return Character.toUpperCase(s.charAt(0))+s.substring(1);
	}
	public static String def(String v, String s){return isEmpty(v)?s:v;}
	public static boolean isBlank(String s){return s==null||s.isBlank();}
	public static boolean isEmpty(String s){return s==null||s.isEmpty();}
	public static boolean isNotEmpty(String s){return !isEmpty(s);}
	public static boolean hasChinese(String str) {return CHINESE.matcher(str).find();}
	public static boolean hasUpperCase(String s) {return s.chars().anyMatch(c->Character.isUpperCase(c));}
	
	public static void main(String[] args) {
		System.out.println(hasUpperCase("123xxx你x"));
	}
	public static boolean isNumber(String s) {return s!=null&&s.chars().allMatch(Character::isDigit);}
}
