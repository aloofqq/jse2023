package com.jse;

public class Obj<T>{
	
	private T t;
	public Obj() {}
	public Obj(T t) {this.t=t;}

	public static <T> Obj<T> of(T t){return new Obj<T>(t);}
	
	public Obj<T> set(T t){this.t=t;return this;}
	
	public T get(){return t;}
}
