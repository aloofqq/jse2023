package com.jse;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

import com.jse.jdbc.Pager;
import com.jse.json.Json;
import com.jse.json.JsonObject;
import com.jse.json.XML;
import com.jse.multipart.MultipartFormData;
import com.jse.util.PatternPool;
import com.jse.util.ReUtil;
import com.jse.web.Action;
import com.jse.web.Tbl;
import com.jse.web.View;
import com.jse.web.Web;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public interface Server {
	void start()throws Exception;
	
	Server tomcat=()->{Js.execute(Io.read(Server.class.getResourceAsStream("/META-INF/jse/tomcat.js")));};
	Server jetty=()->{Js.execute(Io.read(Server.class.getResourceAsStream("/META-INF/jse/jetty.js")));};
	Server jdkserver=()->{
		HttpServer server = HttpServer.create(new InetSocketAddress(80), 0,"/",
				new Action(), new com.sun.net.httpserver.Filter(){
			@Override
			public void doFilter(HttpExchange ex, Chain chain) throws IOException {
				boolean next=true;//是否继续
				if(next)chain.doFilter(ex);
			}

			@Override public String description() {return "before";}
	    	
	    },com.sun.net.httpserver.Filter.afterHandler("after",(ex)->{
			
		}));
	    
	    //静态
	    /*var st=SimpleFileServer.createFileHandler(Path.of("D:\\work\\xjy\\web\\assets"));
	    server.createContext("/assets/",st);*/
	    server.setExecutor(Executors.newVirtualThreadPerTaskExecutor());
	    server.start();
	    Web.init();
	};
	
}
