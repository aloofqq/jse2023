package com.jse;

import java.math.BigDecimal;

/**
 * 数学计算类
 */
public class Maths {

	/** 将值固定在最小值和最大值之间 @param val 值 @param min 最小 @param max 最大 @return 区间 */
	public static double clamp(double val,double min,double max){return Math.min(max,Math.max(val, min));}
	/** 加法 @return BigDecimal */
	public static BigDecimal add(Object a,Object b){return new BigDecimal(a.toString()).add(new BigDecimal(b.toString()));}
	/** 减法 @return BigDecimal */
	public static BigDecimal sub(Object a,Object b){return new BigDecimal(a.toString()).subtract(new BigDecimal(b.toString()));}
	/** 乘法 @return BigDecimal */
	public static BigDecimal mul(Object a,Object b){return new BigDecimal(a.toString()).multiply(new BigDecimal(b.toString()));}
	/** 除法 @return BigDecimal */
	public static BigDecimal div(Object a,Object b){return new BigDecimal(a.toString()).divide(new BigDecimal(b.toString()));}
	/** 相等则返回0，a小返回-1，a大返回1。*/
	public static int compareTo(Object a,Object b) {return new BigDecimal(a.toString()).compareTo(new BigDecimal(b.toString()));}

	/**把给定的总数平均分成N份，返回每份的个数 当除以分数有余数时每份+1 
	 * @param total 总数 
	 * @param count 份数 
	 * @return 每份的个数 */
	public static int part(int total, int count){return part(total, count, true);}
	
	/**把给定的总数平均分成N份，返回每份的个数 
	 * @param total 总数 
	 * @param partCount 份数 
	 * @param isAdd 当除以分数有余数时每份+1 
	 * @return 每份的个数 */
	public static int part(int total, int count, boolean isAdd){int v=total/count;if(isAdd&&total%count==0)v++;return v;}
	
	public static void main(String[] args) {
		
	}
	
}
