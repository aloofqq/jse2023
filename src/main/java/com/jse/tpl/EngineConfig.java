/**
 * Copyright (c) 2011-2023, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jse.tpl;

import java.lang.reflect.Method;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import com.jse.Lang;

/**
 * EngineConfig
 */
public class EngineConfig {
	
	WriterBuffer writerBuffer = new WriterBuffer();
	
	private Map<String, Define> sharedFunctionMap = createSharedFunctionMap();		// new HashMap<String, Define>(512, 0.25F);
	private List<ISource> sharedFunctionSourceList = new ArrayList<ISource>();		// for devMode only
	
	Map<String, Object> sharedObjectMap = null;
	
	private SharedMethodKit sharedMethodKit = new SharedMethodKit();
	
	// 保留指令所在行空白字符的指令
	private Set<String> keepLineBlankDirectives = new HashSet<>();
	private String baseTemplatePath = null;
//	private String encoding = DEFAULT_ENCODING;
	private String datePattern = "yyyy-MM-dd HH:mm";
	
	// 浮点数输出与运算时使用的舍入模式，默认值为 "四舍五入"
	private RoundingMode roundingMode = RoundingMode.HALF_UP;
	
	public EngineConfig() {
		// 内置指令 #() 与 #include() 需要配置，保留指令所在行前后空白字符以及行尾换行字符 '\n'
		setKeepLineBlank("output", true);
		setKeepLineBlank("include", true);
		// Directive
//		addDirective("render", RenderDirective.class, true);
		
		// Add official shared method of Template Engine
		addSharedStaticMethod(Lang.class);
	}
	
	/**
	 * Add shared function with file
	 */
	public void addSharedFunction(String code) {
		// FileSource fileSource = new FileSource(baseTemplatePath, fileName, encoding);
		ISource source = new ISource(code, Tpl.dev);
		doAddSharedFunction(source,source.getId());
	}
	
	private synchronized void doAddSharedFunction(ISource source, String id) {
		Env env = new Env(this);
		new Parser(env, source.getContent(), id).parse();
		addToSharedFunctionMap(sharedFunctionMap, env);
		if (Tpl.dev) {
			sharedFunctionSourceList.add(source);
			env.addSource(source);
		}
	}
	
	/**
	 * Add shared function with files
	 */
	public void addSharedFunction(String... codes) {
		for (String fileName : codes) {
			addSharedFunction(fileName);
		}
	}
	
	/**
	 * Add shared function by string content
	 */
	public void addSharedFunctionByString(String content) {
		// content 中的内容被解析后会存放在 Env 之中，而 StringSource 所对应的
		// Template 对象 isModified() 始终返回 false，所以没有必要对其缓存
		ISource stringSource = new ISource(content, false);
		doAddSharedFunction(stringSource,stringSource.getId());//添加字符串
	}
	
	/**
	 * Add shared function by ISource
	 */
	public void addSharedFunction(ISource source) {
		doAddSharedFunction(source,Lang.uuid());
	}
	
	private void addToSharedFunctionMap(Map<String, Define> sharedFunctionMap, Env env) {
		Map<String, Define> funcMap = env.getFunctionMap();
		for (Entry<String, Define> e : funcMap.entrySet()) {
			if (sharedFunctionMap.containsKey(e.getKey())) {
				throw new IllegalArgumentException("Template function already exists : " + e.getKey());
			}
			Define func = e.getValue();
			if (Tpl.dev) {
				func.setEnvForDevMode(env);
			}
			sharedFunctionMap.put(e.getKey(), func);
		}
	}
	
	/**
	 * Get shared function by Env
	 */
	Define getSharedFunction(String functionName) {
		Define func = sharedFunctionMap.get(functionName);
		if (func == null) {
			/**
			 * 如果 func 最初未定义，但后续在共享模板文件中又被添加进来
			 * 此时在本 if 分支中无法被感知，仍然返回了 null
			 * 
			 * 但共享模板文件会在后续其它的 func 调用时被感知修改并 reload
			 * 所以本 if 分支不考虑处理模板文件中追加 #define 的情况
			 * 
			 * 如果要处理，只能是每次在 func 为 null 时，判断 sharedFunctionSourceList
			 * 中的模板是否被修改过，再重新加载，不优雅
			 */
			return null;
		}
		
		if (Tpl.dev) {
			if (func.isSourceModifiedForDevMode()) {
				synchronized (this) {
					func = sharedFunctionMap.get(functionName);
					if (func.isSourceModifiedForDevMode()) {
						reloadSharedFunctionSourceList();
						func = sharedFunctionMap.get(functionName);
					}
				}
			}
		}
		return func;
	}
	
	/**
	 * Reload shared function source list
	 * 
	 * devMode 要照顾到 sharedFunctionFiles，所以暂不提供
	 * removeSharedFunction(String functionName) 功能
	 * 开发者可直接使用模板注释功能将不需要的 function 直接注释掉
	 */
	private synchronized void reloadSharedFunctionSourceList() {
		Map<String, Define> newMap = createSharedFunctionMap();
		for (int i = 0, size = sharedFunctionSourceList.size(); i < size; i++) {
			ISource source = sharedFunctionSourceList.get(i);
			Env env = new Env(this);
			new Parser(env, source.getContent(),source.getId()).parse();
			addToSharedFunctionMap(newMap, env);
			if (Tpl.dev) {
				env.addSource(source);
			}
		}
		this.sharedFunctionMap = newMap;
	}
	
	private Map<String, Define> createSharedFunctionMap() {
		return new HashMap<String, Define>(512, 0.25F);
	}
	
	public synchronized void addSharedObject(String name, Object object) {
		if (sharedObjectMap == null) {
			sharedObjectMap = new HashMap<String, Object>(64, 0.25F);
		} else if (sharedObjectMap.containsKey(name)) {
			throw new IllegalArgumentException("Shared object already exists: " + name);
		}
		sharedObjectMap.put(name, object);
	}
	
	public Map<String, Object> getSharedObjectMap() {
		return sharedObjectMap;
	}
	
	public synchronized void removeSharedObject(String name) {
		if (sharedObjectMap != null) {
			sharedObjectMap.remove(name);
		}
	}
	
	public void setBufferSize(int bufferSize) {
		writerBuffer.setBufferSize(bufferSize);
	}
	
	public void setReentrantBufferSize(int reentrantBufferSize) {
		writerBuffer.setReentrantBufferSize(reentrantBufferSize);
	}
	
	/**
	 * 配置自己的 WriterBuffer 实现，配置方法：
	 * engine.getEngineConfig().setWriterBuffer(...);
	 */
	public void setWriterBuffer(WriterBuffer writerBuffer) {
		Objects.requireNonNull(writerBuffer, "writerBuffer can not be null");
		this.writerBuffer = writerBuffer;
	}
	
	public void setDatePattern(String datePattern) {
		if (datePattern==null||datePattern.isBlank()) {
			throw new IllegalArgumentException("datePattern can not be blank");
		}
		this.datePattern = datePattern;
	}
	
	public String getDatePattern() {
		return datePattern;
	}
	
	public void setKeepLineBlank(String directiveName, boolean keepLineBlank) {
		if (keepLineBlank) {
			keepLineBlankDirectives.add(directiveName);
		} else {
			keepLineBlankDirectives.remove(directiveName);
		}
	}
	
	public Set<String> getKeepLineBlankDirectives() {
		return keepLineBlankDirectives;
	}
	
	/**
	 * Add shared method from object
	 */
	public void addSharedMethod(Object sharedMethodFromObject) {
		sharedMethodKit.addSharedMethod(sharedMethodFromObject);
	}
	
	/**
	 * Add shared method from class
	 */
	public void addSharedMethod(Class<?> sharedMethodFromClass) {
		sharedMethodKit.addSharedMethod(sharedMethodFromClass);
	}
	
	/**
	 * Add shared static method of Class
	 */
	public void addSharedStaticMethod(Class<?> sharedStaticMethodFromClass) {
		sharedMethodKit.addSharedStaticMethod(sharedStaticMethodFromClass);
	}
	
	/**
	 * Remove shared Method with method name
	 */
	public void removeSharedMethod(String methodName) {
		sharedMethodKit.removeSharedMethod(methodName);
	}
	
	/**
	 * Remove shared Method of the Class
	 */
	public void removeSharedMethod(Class<?> sharedClass) {
		sharedMethodKit.removeSharedMethod(sharedClass);
	}
	
	/**
	 * Remove shared Method
	 */
	public void removeSharedMethod(Method method) {
		sharedMethodKit.removeSharedMethod(method);
	}
	
	public SharedMethodKit getSharedMethodKit() {
		return sharedMethodKit;
	}
	
	/**
	 * 设置 #number 指令与 Arith 中浮点数的舍入规则，默认为 RoundingMode.HALF_UP "四舍五入"
	 */
	public void setRoundingMode(RoundingMode roundingMode) {
		this.roundingMode = roundingMode;
		Arith.setBigDecimalDivideRoundingMode(roundingMode);
	}
	
	public RoundingMode getRoundingMode() {
		return roundingMode;
	}
}





