/**
 * Copyright (c) 2011-2023, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jse.tpl;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Text 输出纯文本块以及使用 "#[[" 与 "]]#" 定义的原样输出块
 */
public class Text extends Stat {
	
	// content、bytes、chars 三者必有一者不为 null
	// 在 OutputStream、Writer 混合模式下 bytes、chars 同时不为null
	private StringBuilder content;
	private Charset charset;
	private byte[] bytes;
	private char[] chars;
	
	// content 初始值在 Lexer 中已确保不为 null
	public Text(StringBuilder content) {
		this.content = compress(content,'\n');//压缩
		this.charset = StandardCharsets.UTF_8;
		this.bytes = null;
		this.chars = null;
	}
	
	private StringBuilder compress(CharSequence c,char separator) {
		int len = c.length();
		StringBuilder ret = new StringBuilder(len);
		char ch;
		boolean hasLineFeed;
		int begin = 0;
		int forward = 0;
		while (forward < len) {// 扫描空白字符
			hasLineFeed = false;
			while (forward < len) {
				ch = c.charAt(forward);
				if (ch <= ' ') {			// 包含换行字符在内的空白字符
					if (ch == '\n') {		// 包含换行字符
						hasLineFeed = true;
					}
					forward++;
				} else {					// 非空白字符
					break ;
				}
			}// 压缩空白字符
			if (begin != forward) {
				if (hasLineFeed) {
					ret.append(separator);
				} else {
					ret.append(' ');
				}
			}
			while (forward < len) {// 复制非空白字符
				ch = c.charAt(forward);
				if (ch > ' ') {
					ret.append(ch);
					forward++;
				} else {
					break ;
				}
			}
			begin = forward;
		}
		return ret;
	}
	
	public void exec(Env env, Scope scope, Writer writer) {
		try {
			writer.write(this);
		} catch (IOException e) {
			throw new TemplateException(e.getMessage(), location, e);
		}
	}
	
	public byte[] getBytes() {
		if (bytes != null) {
			return bytes;
		}
		
		synchronized (this) {
			if (bytes != null) {
				return bytes;
			}
			
			if (content != null) {
				bytes = content.toString().getBytes(charset);
				content = null;
				return bytes;
			} else {
				bytes = new String(chars).getBytes(charset);
				return bytes;
			}
		}
	}
	
	public char[] getChars() {
		if (chars != null) {
			return chars;
		}
		
		synchronized (this) {
			if (chars != null) {
				return chars;
			}
			
			if (content != null) {
				char[] charsTemp = new char[content.length()];
				content.getChars(0, content.length(), charsTemp, 0);
				chars = charsTemp;
				content = null;
				return chars;
			} else {
				String strTemp = new String(bytes, charset);
				char[] charsTemp = new char[strTemp.length()];
				strTemp.getChars(0, strTemp.length(), charsTemp, 0);
				chars = charsTemp;
				return chars;
			}
		}
	}
	
	public boolean isEmpty() {
		if (content != null) {
			return content.length() == 0;
		} else if (bytes != null) {
			return bytes.length == 0;
		} else {
			return chars.length == 0;
		}
	}
	
//	public String getContent() {
//		return text != null ? new String(text) : null;
//	}
	
	public String toString() {
		if (bytes != null) {
			return new String(bytes, charset);
		} else if (chars != null) {
			return new String(chars);
		} else {
			return content.toString();
		}
	}
}



