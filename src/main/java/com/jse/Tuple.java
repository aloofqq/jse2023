package com.jse;

public record Tuple<A,B>(A a,B b){}
