package com.jse;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;

public class Io {

	public static void close(AutoCloseable...acs) {
		try{
			for(var ac:acs) {
				if(ac instanceof Connection c) {
					if(c.getAutoCommit()){c.close();c=null;}//不是自动提交时不关闭
				}else{
					ac.close();ac=null;
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("io close fail!");
		}
	}

	public static String read(Reader reader) {
		try(reader){
			StringWriter sw=new StringWriter();
			reader.transferTo(sw);
			return sw.toString();
		} catch (IOException e) {
			throw new RuntimeException("io reader fail!");
		}
	}

	public static String read(InputStream in) {
		try {
			return new String(in.readAllBytes(),StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new RuntimeException("io input fail!");
		}
	}
	
	public static String readString(Reader reader) {
		StringBuilder sb=new StringBuilder();
        char[] cbuf = new char[8192];
        int len;
        try {
        	while (reader.ready()) {
        		len=reader.read(cbuf);
                sb.append(cbuf, 0, len);
            }
		} catch (IOException e) {e.printStackTrace();
		}
        return sb.toString();
    }
	
	public static byte[] readSocketByte(InputStream in,int lenx) {
        byte[] b=new byte[lenx];//1024可改成任何需要的值  
    	try(BufferedInputStream is=new BufferedInputStream(in)){
    		is.read(b,0,lenx);
    	    return b;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
    }
	
	public static byte[] readBytes(InputStream in) {
        byte[] buffer = new byte[8192];
        int n = 0;
        try(ByteArrayOutputStream out = new ByteArrayOutputStream()){
        	while (-1 != (n = in.read(buffer))) {
                out.write(buffer, 0, n);
            }
            return out.toByteArray();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
    }
	public static String readString(InputStream in) {
		return new String(readBytes(in),StandardCharsets.UTF_8);
	}

	
}
