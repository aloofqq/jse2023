package com.jse.api;

import java.net.URLEncoder;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class Gitee {

	/**
	 * WebHook 签名
	 * @param secret gitee项目的secret
	 * @return
	 * @throws Exception
	 */
	public static String sign(String secret) throws Exception {
		Long timestamp = System.currentTimeMillis();
		String stringToSign = timestamp + "\n" + secret;
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256"));
		byte[] signData = mac.doFinal(stringToSign.getBytes("UTF-8"));
		return URLEncoder.encode(new String(Base64.getEncoder().encode(signData)),"UTF-8");
	}
}
