package com.jse.api;

import java.io.IOException;
import java.net.http.HttpClient.Version;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jse.Http;
import com.jse.Jse;
import com.jse.json.Json;
import com.jse.json.JsonObject;

/**
 * 微信小程序工具类
 * @author dzh
 *
 */
public class WxApp {
	
	public static String qrtype(String type) {
		if(type.equalsIgnoreCase("createqrcode")) {
			return "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=%s";
		}else if(type.equalsIgnoreCase("get")) {
			return "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=%s";
		}else {
			return "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=%s";
		}
	}
	
	public static String token(Map<String,Object> tbl) {
		String access_token=Jse.cache.get("wxapp.access_token","");
		if(access_token.isEmpty()) {
			access_token=Http.get("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s".formatted(tbl.remove("appid"),tbl.remove("secret")));
			Jse.cache.put("wxapp.access_token",access_token,7000000);
		}
		return Json.parse(access_token,Map.class).get("access_token").toString();
	}
	/**
	 * access_token	string		是	接口调用凭证
		scene	string		是	最大32个可见字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~，其它字符请自行编码为合法字符（因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式）
		page	string	主页	否	必须是已经发布的小程序存在的页面（否则报错），例如 pages/index/index, 根路径前不要填加 /,不能携带参数（参数请放在scene字段里），如果不填写这个字段，默认跳主页面
	 * @param args
	 * @throws IOException 
	 */
	public static void qrcode(Map<String,Object> tbl) {
		String token=token(tbl);
		String qrtype=tbl.remove("method").toString();
		String url=qrtype(qrtype).formatted(token);
		String file=tbl.remove("file").toString();
		tbl.remove("appid");
		tbl.remove("secret");
		var headers=new HashMap<String,String>(Map.of("Content-Type","application/json"));
		Http.send(Version.HTTP_1_1,"POST",url,Json.toJson(tbl),30000,headers,true,Paths.get(file));
	}
	
	public static void qrcode(List<Map<String,Object>> list) {
		for (Map<String,Object> tbl : list) {
			qrcode(tbl);
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		for (int i = 0; i < 1; i++) {
			JsonObject tbl=new JsonObject();
			//公共参数
			tbl.put("appid", "wx7fe1762f4cbb38ee");
			tbl.put("secret", "8f9a49d54e93a58fa1b3c1161eb76b1e");
			//微信api对应方法参数 那些否的也可以
			tbl.put("scene", "1,info,info,1");
			tbl.put("page", "pages/tabbar/index");
			//本插件对应方法
			tbl.put("method", "getUnlimited");
			tbl.put("file", "d:/"+i+".jpg");
			qrcode(tbl);
		}
		
	}
}
