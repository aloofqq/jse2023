package com.jse.api;

import java.util.Map;

import com.jse.Http;

public class WxPay {
	/**
	 * @param param 
	{
		"mchid": "1900006XXX",
		"out_trade_no": "1217752501201407033233368318",
		"appid": "wxdace645e0bc2cXXX",
		"description": "Image形象店-深圳腾大-QQ公仔",
		"notify_url": "https://www.weixin.qq.com/wxpay/pay.php",
		"amount": {
			"total": 1,
			"currency": "CNY"
		},
		"payer": {
			"openid": "o4GgauInH_RCEdvrrNGrntXDuXXX"
		}
	}
	 */
	public static String pay(Map param) {
		String type=param.getOrDefault("type","jsapi").toString();//app,h5,native
		return Http.postJson("https://api.mch.weixin.qq.com/v3/pay/transactions/"+type,param);
	}
	public static String jsquery(Map param) {
		if(param.containsKey("transaction_id")){
			Http.get("https://api.mch.weixin.qq.com/v3/pay/transactions/id/%s?mchid=%s"
					.formatted(param.get("transaction_id"),param.get("mchid")));
		}
		return Http.get("https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/%s?mchid=%s"
				.formatted(param.get("transaction_id"),param.get("mchid")));
	}
	/** JSAPI关闭订单 https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_1_3.shtml */
	public static String jsclose(String out_trade_no,String mchid) {
		return Http.postJson("https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/%s/close"
				.formatted(out_trade_no),
				Map.of("mchid",mchid));
	}
	public static String jsbill(String date,String mchid) {
		return Http.postJson("https://api.mch.weixin.qq.com/v3/bill/tradebill?bill_date=%s"
				.formatted(date),
				Map.of("mchid",mchid));
	}
}
