var global = this;
if(!console){var console={debug:print,warn:print,log:print,error:print,trace:print};global.console=console;
/*Object.assign = function (t) {
  for (var s, i = 1, n = arguments.length; i < n; i++) {
    s = arguments[i];
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
      t[p] = s[p];
  }
  return t;
};*/
}
global.process={env:{},nextTick:(fn)=>global.setTimeout(fn, 0)};
if(!module){var module={exports:global};global.module=module;}
var alert=print;
var System=Java.type("java.lang.System");
var URLEncoder=Java.type("java.net.URLEncoder");
var URLDecoder=Java.type("java.net.URLDecoder");
var ArrayList=Java.type("java.util.ArrayList");
var LinkedList=Java.type("java.util.LinkedList");
var List=Java.type("java.util.List");
var Arrays=Java.type("java.util.Arrays");
var HashMap=Java.type("java.util.HashMap");
var HashSet=Java.type("java.util.HashSet");
var Calendar=Java.type("java.util.Calendar");
var File=Java.type("java.io.File");
var Path=Java.type("java.nio.file.Path");
var FileInputStream=Java.type("java.io.FileInputStream");
var FileOutputStream=Java.type("java.io.FileOutputStream");
var Jse=Java.type("com.jse.Jse");
var Ioc=Java.type("com.jse.Ioc");
var Job=Java.type("com.jse.Job");
var setTimeout=Job.setTimeout;
var setInterval=Job.setInterval;
var clearTimeout=Job.clearTimeout;
var clearInterval=Job.clearInterval;
var sleep=java.lang.Thread.sleep;
var Fs=Java.type("com.jse.Fs");
var Lang=Java.type("com.jse.Lang");
var Trees=Java.type("com.jse.Trees");
var Cnd=Java.type("com.jse.jdbc.Cnd");
var Pager=Java.type("com.jse.jdbc.Pager")
var JsonObject=Java.type("com.jse.json.JsonObject");
var JsonArray=Java.type("com.jse.json.JsonArray");
var Json=Java.type("com.jse.json.Json");
var Http=Java.type("com.jse.Http");
var Times=Java.type("com.jse.Times");
var toJson=(s)=>Json.toJson(s);
var foJson=(s)=>Json.parse(s);
function asposelicense(x){let License=Java.type("com.aspose.words.License");new License().setLicense(x);}
function asposeTo(inFile,outFile){try{let Document=Java.type("com.aspose.words.Document");new Document(inFile).save(outFile);}catch(e){print(e)}}
function include(f){return Fs.read('webapp',f)}
var isEmpty=function(x){return Lang.isEmpty(x);}
var attr=function(n,v){if(arguments.length==1){return Web.attr(n);}else Web.attr(n,v);}
var sattr=function(n,v){if(arguments.length==1){return Web.sattr(n);}else Web.sattr(n,v);}
var user=function(){return sattr("user")}
var data=function(n,v){if(arguments.length==1){return Web.data(n);}else Web.data(n,v);}
var ip=function(){return Web.ip();}
load("nashorn:parser.js");//解析api
function tpljs(t,m){
  let s='(function(){\n';
  for(let k in m){
	 let v=m[k];
	 if(typeof(v)=='string'){v=`'${v}'`}
	 if(global[k]==null)
	 s+=`let ${k.replace('\.','_')}=${v}\n`;
  }
  s+='return `'+t+'`})()'
  return eval(s);
}
function parsejs(code){return JSON.stringify(parse(code).body)}
function parsejsf(code){
	let arr={};
	for(let x of parse(code).body){
		if(x.type=="FunctionDeclaration"){
			arr[x.id.name]={"ret":x.body.body.length>0,"param":x.params.length};
		}
	}
	return JSON.stringify(arr)
}
function parsejsx(code){
	let arr=[];
	for(let x of parse(code).body){
		if(x.type=="FunctionDeclaration"){
			arr.push({"type":"method","name":x.id.name,"ret":x.body.body.length>0,"param":x.params.length})
		}else if(x.type=="VariableDeclaration"){
			arr.push({"type":"var","name":x.declarations[0].id.name,"value":x.declarations[0].init.value})
		}else{
			arr.push(x)
		}
	}
	return JSON.stringify(arr)}