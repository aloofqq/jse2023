var hrface={
	engine:new com.arcsoft.face.FaceEngine(conf["hrface.path"]),
	isinit:false,
	init(){
		if(!this.isinit){
		var info=new com.arcsoft.face.ActiveDeviceInfo();
		var code = this.engine.getActiveDeviceInfo(info);
		console.log("hrface engine info code:"+code)
		console.log("info:"+info.getDeviceInfo());
		if(conf.containsKey("hrface.activefile")){//离线激活
		code=this.engine.activeOffline(conf["hrface.activefile"]);
		console.log("hrface engine activeOffline code:"+code)
		}else{
		code=this.engine.activeOnline(conf["hrface.appid"],conf["hrface.sdkkey"],conf["hrface.activekey"]);
		console.log("hrface engine activeOnline code:"+code)
		}
		var engineConfiguration = new com.arcsoft.face.EngineConfiguration();
        engineConfiguration.setDetectMode(com.arcsoft.face.enums.DetectMode.ASF_DETECT_MODE_IMAGE);
        engineConfiguration.setDetectFaceOrientPriority(com.arcsoft.face.enums.DetectOrient.ASF_OP_ALL_OUT);
        engineConfiguration.setDetectFaceMaxNum(10);
        var functionConfiguration = new com.arcsoft.face.FunctionConfiguration();
        functionConfiguration.setSupportAge(true);
        functionConfiguration.setSupportFaceDetect(true);
        functionConfiguration.setSupportFaceRecognition(true);
        functionConfiguration.setSupportGender(true);
        functionConfiguration.setSupportLiveness(true);
        functionConfiguration.setSupportIRLiveness(true);
        functionConfiguration.setSupportImageQuality(true);
        functionConfiguration.setSupportMaskDetect(true);
        functionConfiguration.setSupportUpdateFaceData(true);
        engineConfiguration.setFunctionConfiguration(functionConfiguration);
        code = this.engine.init(engineConfiguration);//初始化引擎
        console.log("hrface init code:" + code);
		}
	},
	info(f){//file|inputstream|byte[]:new java.io.File("D:\\upload\\1.png") 人脸检测
		var imageInfo = com.arcsoft.face.toolkit.ImageFactory.getRGBData(new java.io.File(f));
        var faceInfoList = new java.util.ArrayList();
        var code = this.engine.detectFaces(imageInfo, faceInfoList);
        return faceInfoList.get(0);
	},
	token(f){//特征提取
		var imageInfo = com.arcsoft.face.toolkit.ImageFactory.getRGBData(new java.io.File(f));
        var faceInfoList = new java.util.ArrayList();
        var code = this.engine.detectFaces(imageInfo, faceInfoList);
        if(code == 0) {
            let faceFeature = new com.arcsoft.face.FaceFeature();
            code = this.engine.extractFaceFeature(imageInfo, faceInfoList.get(0),
            com.arcsoft.face.enums.ExtractType.REGISTER, 0, faceFeature);
            if(code == 0) {
            	return java.util.Base64.getEncoder().encodeToString(faceFeature.getFeatureData());
            }
        }
		return null;
	},
	isEq(token,token1) {
		try {
			let targetFaceFeature = new com.arcsoft.face.FaceFeature();
			targetFaceFeature.setFeatureData(java.util.Base64.getMimeDecoder().decode(token));
			let sourceFaceFeature = new com.arcsoft.face.FaceFeature();
			sourceFaceFeature.setFeatureData(java.util.Base64.getMimeDecoder().decode(token1));
			let faceSimilar = new com.arcsoft.face.FaceSimilar();
			let errorCode = this.engine.compareFaceFeature(targetFaceFeature, sourceFaceFeature, faceSimilar);
			print("特征比对errorCode:" + errorCode);
			if (errorCode == 0) {
	           print("人脸相似度：" + faceSimilar.getScore());
				return faceSimilar.getScore() > 0.75;
			}
		} catch (e) {
			e.printStackTrace();
		}
		return false;
	}
}