function gitee(tbl){
	if(tbl.password==conf.getString("jse.gitee.password","jse")){//密码正确1
		if(tbl.commits){
			tbl.commits.forEach(function(commit){
				commit.removed.forEach(function(modif){//
					let path1=modif;
					if(modif.startsWith("src/")){
						path1=Jse.jspath()+modif.substring(4);
					}else if(modif.startsWith("web/")){
						path1=Jse.webapp()+modif.substring(4);
					}
					Fs.delete(path1)
				})
				commit.added.forEach(function(modif){//
					giteeupdate(modif);
				})
				commit.modified.forEach(function(modif){//
					giteeupdate(modif);
				})
			})
			return {code:0,msg:"update code ok!"}
		}
	}
	return {code:-1,msg:"not update!"}
}
function giteeupdate(modif){
 let path1=modif;
 if(modif.startsWith("src/")){
  path1=Jse.jspath()+modif.substring(4);
 }else if(modif.startsWith("web/")){
  path1=Jse.webapp+modif.substring(4);
 }
 body=Http.get(`https://gitee.com/api/v5/repos/${conf.get('gitee.user')}/${conf.get('gitee.project')}/contents/${modif}?access_token=${conf.get('gitee.token')}&ref=master`)
 var cs=JSON.parse(body).content;
 Fs.write(path1,java.util.Base64.getDecoder().decode(cs));
}