(function(){
  var tomcat = new org.apache.catalina.startup.Tomcat();
  var context = tomcat.addContext("","");
  context.addServletContainerInitializer(new com.jse.jakarta.Web(),null);
  var connector = new org.apache.catalina.connector.Connector();
  var protocol = connector.getProtocolHandler();
  protocol.setPort(conf.get("server.port")||80);
  protocol.setExecutor(java.util.concurrent.Executors.newVirtualThreadPerTaskExecutor());
  tomcat.getService().addConnector(connector);
  tomcat.start();
})()