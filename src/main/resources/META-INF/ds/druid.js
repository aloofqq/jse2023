(function(){
	var dataSource=new com.alibaba.druid.pool.DruidDataSource();
	ds.forEach(function(k,v){
		dataSource.addConnectionProperty(k, v.toString());
	});
	if (null == dataSource.getValidationQuery()) {
		// 在validationQuery未设置的情况下，以下三项设置都将无效
		dataSource.setTestOnBorrow(false);
		dataSource.setTestOnReturn(false);
		dataSource.setTestWhileIdle(false);
	}
	return dataSource;
})()