(function(){
	var poolProps = new org.apache.tomcat.jdbc.pool.PoolProperties();
	poolProps.setUrl(ds.removeString("jdbcUrl","url"));
	poolProps.setDriverClassName(ds.removeString("driverClassName","driver-class-name","driver"));
	poolProps.setUsername(ds.removeString("username","user"));
	poolProps.setPassword(ds.removeString("password","pwd"));
	return new org.apache.tomcat.jdbc.pool.DataSource(poolProps);
})()
