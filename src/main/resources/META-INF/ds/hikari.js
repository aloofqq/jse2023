(function(){
	var config=new com.zaxxer.hikari.HikariConfig();
	  config.setJdbcUrl(ds.removeString("jdbcUrl","url"));
	  config.setDriverClassName(ds.removeString("driverClassName","driver-class-name","driver"));
	  config.setUsername(ds.removeString("username","user"));
	  config.setPassword(ds.removeString("password","pwd"));
	  ds.forEach(function(k,v){
		  config.addDataSourceProperty(k.startsWith("dataSource.")?k:"dataSource."+k, v);
	  });
	  return new com.zaxxer.hikari.HikariDataSource(config);
})()