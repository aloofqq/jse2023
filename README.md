###从标准jdk启用graaljs

```
<!-- jdk为graal时忽略 -->
	<!-- https://mvnrepository.com/artifact/org.graalvm.js/js -->
	<dependency>
	    <groupId>org.graalvm.js</groupId>
	    <artifactId>js</artifactId>
	    <version>23.0.1</version>
	</dependency>
	<!-- https://mvnrepository.com/artifact/org.graalvm.js/js-scriptengine -->
	<dependency>
	    <groupId>org.graalvm.js</groupId>
	    <artifactId>js-scriptengine</artifactId>
	    <version>23.0.1</version>
	</dependency>
	<!-- https://mvnrepository.com/artifact/org.graalvm.sdk/graal-sdk -->
	<dependency>
	    <groupId>org.graalvm.sdk</groupId>
	    <artifactId>graal-sdk</artifactId>
	    <version>23.0.1</version>
	</dependency>
	<!-- https://mvnrepository.com/artifact/org.graalvm.regex/regex -->
	<dependency>
	    <groupId>org.graalvm.regex</groupId>
	    <artifactId>regex</artifactId>
	    <version>23.0.1</version>
	</dependency>
	<!-- https://mvnrepository.com/artifact/org.graalvm.truffle/truffle-api -->
	<dependency>
	    <groupId>org.graalvm.truffle</groupId>
	    <artifactId>truffle-api</artifactId>
	    <version>23.0.1</version>
	</dependency>
```