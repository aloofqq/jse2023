package com.jse.test;

import java.nio.file.Path;

import com.jse.nashorn.api.tree.FunctionDeclarationTree;
import com.jse.nashorn.api.tree.Parser;
import com.jse.nashorn.api.tree.SimpleTreeVisitorES6;

public class JsTest {
     public static void main(String[] args) throws Exception {
         var cut = Parser.create().parse(Path.of("d:/1.js"),
             (d) -> { System.out.println(d); });
         if (cut != null) {
             cut.accept(new SimpleTreeVisitorES6<Void, Void>() {
				@Override
				public Void visitFunctionDeclaration(FunctionDeclarationTree node, Void r) {
					System.out.println("fund"+node.getName().getName());
					return super.visitFunctionDeclaration(node, r);
				}
                 
             }, null);
         }
     }
 }