package com.jse.test;

import java.lang.System.Logger.Level;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.jse.Ioc;
import com.jse.Jse;
import com.jse.Times;
import com.jse.anno.Table;
import com.jse.jdbc.Cnd;
import com.jse.jdbc.Dao;
import com.jse.jdbc.Jdbc;

public class JdbcTest {
//	private String driverClassName = "org.h2.Driver"; // 数据库连接驱动
//	private String url = "jdbc:h2:file:./data/demo;AUTO_SERVER=TRUE"; // 数据库连接url
//	private String user = "sa"; // 数据库连接user
//	private String password = ""; // 数据库连接password
//	private int minConnection; // 数据库连接池最小连接数,即连接池创建后的初始数量
//	private int maxConnection; // 数据库连接池最大连接数
//	private long timeoutValue; // 连接的最大空闲时间
//	private long waitTime; // 取得连接的最大等待时间
//	private int incrementalConnections = 5; //连接池自动增加连接的数量
//	private String connections = "jdbc"; //连接池自动增加连接的数量
	@Table(value = "test")
	static class Test1{
		private Integer id;
		private String name;
		private int age;
		
		public Test1(Integer id, String name, int age) {
			this.id = id;
			this.name = name;
			this.age = age;
		}

		public Test1(String name, int age) {
			this.name = name;
			this.age = age;
		}
		
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		Properties prop=new Properties();
		prop.put("jdbc.dataSource.url", "jdbc:mysql://127.0.0.1:3306/iepsy");
		prop.put("jdbc.dataSource.username", "root");
		prop.put("jdbc.dataSource.password", "!QAZ@WSX1qaz2wsx");
		Jse.conf.putAll(prop);
		Jse.LEVEL=Level.DEBUG;
		Jdbc.start();
		Dao dao=Ioc.get("dao",Dao.class);
//		ArrayList<Object[]> params=new ArrayList<>();
//		for (int i = 0; i < 10; i++) {
//			params.add(new Object[] {"zs"+i,10+i});
//		}
////		params.add(new Object[] {"zs","xxx"});
//		var line=dao.batch("INSERT INTO test(name,age) VALUES(?, ?)",params);
//		System.out.println(Arrays.toString(line));
//		dao.update("test",Map.of("name","张三"),Cnd.where("id","=",120));
		System.out.println(Times.parse("2016-01-30 15:32:11"));
		System.out.println(dao.query("users",Cnd.where("reg_time","between",List.of(
				Times.parse("2016-01-30 15:32:11"),Times.parse("2016-02-03 15:32:11")
				))));
//		System.out.println(dao.queryForMap(sql,10));
		
//		Jdbc.tx(1,()->{
//			var m=Lang.ofMap("table","test",
//					"name","zs","age",20);
//			dao.insert(m);
//			m.put("name","ls");
//			m.put("table","test");
//			dao.insert(m);
//			m.put("name","ww");
//			m.put("age",null);
//			m.put("table","test");
//			dao.insert(m);
//		});
//		DaoUser p=new DaoUser();
//		p.setId(122);
//		p.setName("mm");
//		p.setAge(22);
//		try {
////			String sql="select * from app_cate where id < ?";
//			var kwprjslist = dao.query("kw_prj", Cnd.where("1", "=", 1).desc("id"));
////			kwprjslist.forEach(x->{
////			System.out.println(x);
////		});
////			var kwprjslist=new ArrayList();
//			System.out.println(kwprjslist.size());
////			dao.insert(p);
//		} catch (Exception e) {
//			e.printStackTrace();
//			if(e instanceof RuntimeException re) {
//				if(e.getCause()!=null) {
//					if(e.getCause() instanceof SQLException se) {
//						System.out.println("sqlexecption code:"+se.getErrorCode()+" state:"+se.getSQLState()+
//								" msg:"+se.getMessage());
//					}
//				}
//			}
//			
////			e.printStackTrace();
//		}
//		Jdbc.tx(1,()->{
//			var m=Lang.ofMap("table","test",
//					"name","zs","age",20);
//			dao.insert(m);
//			m.put("name","ls");
//			m.put("age",22);
//			m.put("table","test");
//			dao.insert(m);
//			System.out.println(dao.queryForMap("select * from app_cate where id < ?",10));
//			m.put("name","ww");
//			m.put("age",null);
//			m.put("table","test");
//			dao.insert(m);
//		});
//		dao.insert(Lang.ofMap("table","test","name","zs1","age",20));
//		dao.update(Lang.ofMap("table","test","name","zs1","age",20,"id",61));

		//云数据库一般不支持xdevapi
//		String url="mysqlx://rm-uf60bm04v19xbiaycjo.mysql.rds.aliyuncs.com:33060/iepsy?user=root&password=!QAZ@WSX1qaz2wsx";
//		
//		ClientFactory cf = new ClientFactory(); 
//
//		//Obtain Client from ClientFactory
//		Client cli = cf.getClient(url, "{\"pooling\":{\"enabled\":true, \"maxSize\":8,\"maxIdleTime\":30000, \"queueTimeout\":10000} }");
//		var mySession=cli.getSession();
////		Schema myDb = mySession.getSchema("test");
//		var schemaList = mySession.getSchemas();
//
//		System.out.println("Available schemas in this session:");
//
//		// Loop over all available schemas and print their name
//		for (Schema schema : schemaList) {
//		System.out.println(schema.getName());
//		}
//
//		mySession.close();
	}
}

