package com.jse.ee;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import jakarta.servlet.http.HttpSession;

public class SessionManager {
	
	public final static Map<String,HttpSession> SESSIONS=new ConcurrentHashMap<>();

	public static HttpSession get(String id) {
		return SESSIONS.get(id);
	}

}
