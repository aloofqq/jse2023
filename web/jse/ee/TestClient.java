package com.jse.ee;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class TestClient {

	public static void main(String[] args) {
        // TODO Auto-generated method stub
        String msg = "rpc:test";
         
        try {
            //创建一个Socket，跟服务器的8080端口链接
            Socket socket = new Socket("127.0.0.1",80);
            //使用PrintWriter和BufferedReader进行读写数据
            PrintWriter pw = new PrintWriter(socket.getOutputStream());
            //发送数据
            pw.write(msg);
            pw.write("\r\n");
            pw.flush();
            //接收数据
            var is = new ObjectInputStream(socket.getInputStream());
            Test user = (Test) is.readObject();
            System.out.println(user);
            user.test();
            //关闭资源
            pw.close();
            is.close();
            socket.close();
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
       
}
