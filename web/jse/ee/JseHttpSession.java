package com.jse.ee;

import java.util.Collections;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpSession;

public class JseHttpSession implements HttpSession {

	private ConcurrentHashMap<String,Object> attributes=new ConcurrentHashMap<>();
	private String id;
	private long lastAccessedTime;
	private long creationTime;
	
	public JseHttpSession(String id) {
		this.id=id;
		this.creationTime=System.currentTimeMillis();
	}
	
	@Override
	public long getCreationTime() {
		return creationTime;
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getLastAccessedTime() {
		// TODO Auto-generated method stub
		return lastAccessedTime;
	}

	@Override
	public ServletContext getServletContext() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setMaxInactiveInterval(int interval) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getMaxInactiveInterval() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getAttribute(String name) {return attributes.get(name);}

	@Override
	public Enumeration<String> getAttributeNames() {return Collections.enumeration(attributes.keySet());}

	@Override
	public void setAttribute(String name, Object value) {attributes.put(name, value);}

	@Override
	public void removeAttribute(String name) {attributes.remove(name);}

	@Override
	public void invalidate() {
		attributes.clear();
	}

	@Override
	public boolean isNew() {
		// TODO Auto-generated method stub
		return false;
	}

}
