package com.jse.ee;

import java.io.IOException;
import java.io.OutputStream;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.WriteListener;

public class JseServletOutputStream extends ServletOutputStream {

	private OutputStream out;
	
	public JseServletOutputStream(OutputStream out) {
		this.out=out;
	}
	
	@Override
	public boolean isReady() {//是否可以在不阻塞的情况下写入数据
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setWriteListener(WriteListener writeListener) {
		// https://houbb.github.io/2021/09/09/java-servlet3-05-response
		
	}

	@Override
	public void write(int b) throws IOException {
		out.write(b);
	}

	@Override
	public void flush() throws IOException {
		out.flush();
	}

	@Override
	public void close() throws IOException {
		out.close();
	}
	

}
