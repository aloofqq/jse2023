package com.jse.ee;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

public class SimpleJmx {
	//-Dcom.sun.management.jmxremote.authenticate=false 
	//-Dcom.sun.management.jmxremote.port=9996 
	//-Dcom.sun.management.jmxremote.ssl=false
    public static void main(String args[]) throws Exception {
        ObjectName objectName = new ObjectName("com.jmx.test.basic:name=simple");
        Simple simple = new Simple();
        simple.setName("someName");
        MBeanServer server = ManagementFactory.getPlatformMBeanServer();
        
        server.registerMBean(simple, objectName);
        int counter = 10;
        while (counter > 0) {
            Thread.sleep(1000);
            System.out.println("JMX server running...");
            counter--;
        }
    }

    public static interface SimpleMBean {
        String getName();

        void setName(String name);

        String print();
    }

    static class Simple implements SimpleMBean {
        private String name;

        @Override
        public String getName() {
            return name;
        }

        @Override
        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String print() {
            return "Print output " + name;
        }
    }
}