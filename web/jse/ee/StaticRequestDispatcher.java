package com.jse.jakarta;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class StaticRequestDispatcher implements RequestDispatcher {

	@Override
	public void forward(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		request.setAttribute("$static",true);//针对自定义服务器
		HttpServletRequest req=(HttpServletRequest)request;
		HttpServletResponse resp=(HttpServletResponse)response;
		View.staticforward.render(req,resp,null);
	}

	@Override
	public void include(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

}
