module jse{
	requires java.net.http;
	requires java.scripting;
	requires java.desktop;
	requires java.compiler;
	requires jdk.unsupported;
	
	requires org.graalvm.polyglot;
	requires org.apache.tomcat.embed.core;
}