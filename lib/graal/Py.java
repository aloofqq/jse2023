package com.jse;

import javax.script.Invocable;
import javax.script.ScriptException;

import org.graalvm.polyglot.Context;

import com.jse.graal.PythonEngineFactory;

public class Py {

	public static void main(String[] args) throws ScriptException, NoSuchMethodException {
//		try(Context context=Context.create()){
//			context.eval("python","""
//					for i in range(1,5):
//					    for j in range(1,5):
//					        for k in range(1,5):
//					            if( i != k ) and (i != j) and (j != k):
//					                print (i,j,k)
//					""");
//		}
		PythonEngineFactory pf=new PythonEngineFactory();
		pf.getScriptEngine().eval("""
				def main( str='def' ):
				   return str+'ok'
				""");
		var a=pf.getScriptEngine().eval("""
				main('xxx')
				""");
		System.out.println(a);
	}
}
